﻿namespace ProjectOCR
{
    partial class Program
    {
        public class Product
        {
            public double sugar { get; set; }
            public double fat { get; set; }
            public double carbohydrates { get; set; }
            public double mass { get; set; }
            public Product(double mass, double sugar = 0, double fat = 0, double carbohydrates = 0)
            {
                this.carbohydrates = carbohydrates * mass / 100;
                this.fat = fat * mass / 100;
                this.sugar = sugar * mass / 100;
                this.mass = mass;
            }
            public string getProductInfo()
            {
                return @"Sugar: " + sugar + " g \nFat: " + fat + " g\nCarbohydrates: " + carbohydrates + " g\n";
            }
        }
    }
}
