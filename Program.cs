﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Drawing;
using System.Threading;
using System.IO.Compression;
using System.Globalization;

namespace ProjectOCR
{
    partial class Program
    {

        //https://cloud.google.com/vision/docs/drag-and-drop OCR used to read data from photos;
        private static void displayMenu()
        {
            Console.Clear();
            Console.WriteLine("----------MENU----------");
            Console.WriteLine("1.ADD NEW PRODUCT");
            Console.WriteLine("2.DISPLAY PRODUCTS");
            Console.WriteLine("3.DISPLAY TOTAL OF PRODUCTS");
            Console.WriteLine("0.EXIT");
        }
        private static string prepareRequestBody(string imageBase64)
        {
            return @"{""requests"": [{""features"":[{""type"": ""TEXT_DETECTION"",""maxResults"": 100}],""image"": {""content"":""" + imageBase64 +
                @"""},""imageContext"": {""cropHintsParams"": {""aspectRatios"": [0.8,1,1.2]}}}]}";
        }
        private static byte[] getBase64ByteArrayFromFile(string path)
        {
            string base64String;
            using (Image image = Image.FromFile(path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    base64String = Convert.ToBase64String(imageBytes);
                }
            }


            return Encoding.ASCII.GetBytes(prepareRequestBody(base64String));
        }
        private static HttpWebRequest prepareRequest(string filePath)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest httpWebRequest = WebRequest.CreateHttp("https://cxl-services.appspot.com/proxy?url=https%3A%2F%2Fvision.googleapis.com%2Fv1%2Fimages%3Aannotate");
            httpWebRequest.Headers.Add("Accept-Encoding", "gzip");
            httpWebRequest.Headers.Add("Origin", "https://cloud.google.com");
            httpWebRequest.Headers[HttpRequestHeader.Authorization] = "cxl-services.appspot.com";
            httpWebRequest.Headers[HttpRequestHeader.AcceptLanguage] = "pl-PL,pl;q=0.9,en-US;q=0.8,en;q=0.7";
            httpWebRequest.Accept = "*/*";
            httpWebRequest.ContentType = "text/plain;charset=UTF-8";
            httpWebRequest.Referer = "https://cloud.google.com/vision/docs/drag-and-drop";
            httpWebRequest.UserAgent = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36";
            httpWebRequest.Method = "POST";

            using (var stream = httpWebRequest.GetRequestStream())
            {
                byte[] data = getBase64ByteArrayFromFile(filePath);
                stream.Write(data, 0, data.Length);
            }

            return httpWebRequest;
        }
        private static string getGoogleOCRText(HttpWebRequest httpWebRequest)
        {
            string text = "";
            try
            {
                HttpWebResponse httpResponse;
                using (httpResponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (Stream stream = httpResponse.GetResponseStream())
                    using (MemoryStream ms = new MemoryStream())
                    using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                    {
                        using (Stream csStream = new GZipStream(stream, CompressionMode.Decompress))
                        {
                            byte[] buffer = new byte[16 * 1024];
                            int read;
                            while ((read = csStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                ms.Write(buffer, 0, read);
                            }
                            byte[] bytes = ms.ToArray();
                            text = Encoding.UTF8.GetString(bytes);
                        }
                    }
                    httpResponse.Close();
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        Console.WriteLine("HTTP Status Code: " + (int)response.StatusCode);
                    }
                }
            }
            return text;
        }
        private static string RemoveDiacritics(string s)
        {
            string asciiEquivalents = Encoding.ASCII.GetString(
                         Encoding.GetEncoding("Cyrillic").GetBytes(s)
                     );

            return asciiEquivalents;
        }
        private static string RemoveIncorrentCharacters(string s)
        {
            string result = "";
            foreach (char c in s)
            {
                if ((c >= '0') && (c <= '9') || c == '.' || c == ',')
                {
                    result += c;
                }
            }
            return result;
        }
        private static double ConvertToDouble(string s)
        {
            char systemSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            double result = 0;
            try
            {
                if (s != null)
                    if (!s.Contains(","))
                        result = double.Parse(s, CultureInfo.InvariantCulture);
                    else
                        result = Convert.ToDouble(s.Replace(".", systemSeparator.ToString()).Replace(",", systemSeparator.ToString()));
            }
            catch (Exception e)
            {
                try
                {
                    result = Convert.ToDouble(s);
                }
                catch
                {
                    try
                    {
                        result = Convert.ToDouble(s.Replace(",", ";").Replace(".", ",").Replace(";", "."));
                    }
                    catch
                    {
                        throw new Exception("Wrong string-to-double format");
                    }
                }
            }
            return result;
        }
        static void Main(string[] args)
        {
            ProductList productList = new ProductList();
            int op = -1;
            double mass = 0;
            do
            {
                displayMenu();
                try
                {
                    op = Int32.Parse(Console.ReadLine());
                }
                catch (System.FormatException)
                {
                    Console.WriteLine("Error input! Try again");
                }

                switch (op)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("-----ADD NEW PRODUCT-----");
                        Console.WriteLine("Set Filename: ");
                        string path = Path.GetFullPath(Console.ReadLine());
                        Console.WriteLine("Set mass [g]: ");
                        try
                        {
                            mass = Double.Parse(Console.ReadLine());
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Error input! Added 0g of your product");
                        }
                        string text = "";
                        text = getGoogleOCRText(prepareRequest(path));
                        OCRComponentList ocrList = new OCRComponentList();
                        ocrList.Add(text);
                        string[] ingredients = { "cukry", "tluszcz", "weglowodany" };
                        productList.Add(new Product(mass, ocrList.GetPropertyValue(ingredients[0]), ocrList.GetPropertyValue(ingredients[1]), ocrList.GetPropertyValue(ingredients[2])));
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("-----DISPLAY PRODUCTS-----");
                        Console.WriteLine(productList.DisplayAllProducts());
                        Console.ReadLine();
                        break;
                    case 3:
                        Console.Clear();
                        Console.WriteLine("-----TOTAL PRODUCTS-----");
                        Console.WriteLine(productList.CalculateProductsTotal().getProductInfo());
                        Console.ReadLine();
                        break;
                    default:
                        break;

                }
            }
            while (op != 0);
        }
    }
}
