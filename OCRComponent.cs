﻿using System.Drawing;

namespace ProjectOCR
{
    partial class Program
    {
        public class OCRComponent
        {
            public OCRComponent(string description, Point point1, Point point2, Point point3, Point point4)
            {
                this.description = description;
                this.point1 = point1;
                this.point2 = point2;
                this.point3 = point3;
                this.point4 = point4;
            }

            public string description { get; set; }
            public Point point1 { get; set; }
            public Point point2 { get; set; }
            public Point point3 { get; set; }
            public Point point4 { get; set; }
        }


    }
}
