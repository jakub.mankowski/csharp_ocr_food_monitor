﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Newtonsoft.Json.Linq;

namespace ProjectOCR
{
    partial class Program
    {
        public class OCRComponentList
        {
            public List<OCRComponent> listOfComponents { get; set; }
            public OCRComponentList()
            {
                listOfComponents = new List<OCRComponent>();
            }
            public void Add(string json)
            {
                dynamic element = JObject.Parse(RemoveDiacritics(json));
                try
                {
                    string stringText = "";
                    int i = 1;
                    while ((stringText = element.responses[0].textAnnotations[i].description) != null)
                    {
                        int px1 = 0, px2 = 0, px3 = 0, px4 = 0, py1 = 0, py2 = 0, py3 = 0, py4 = 0;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[0].x != null)
                            px1 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[0].x;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[1].x != null)
                            px2 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[1].x;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[2].x != null)
                            px3 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[2].x;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[3].x != null)
                            px4 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[3].x;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[0].y != null)
                            py1 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[0].y;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[1].y != null)
                            py2 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[1].y;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[2].y != null)
                            py3 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[2].y;
                        if (element.responses[0].textAnnotations[i].boundingPoly.vertices[3].y != null)
                            py4 = (int)element.responses[0].textAnnotations[i].boundingPoly.vertices[3].y;

                        listOfComponents.Add(new OCRComponent(
                            stringText,
                            new Point(px1, py1),
                            new Point(px2, py2),
                            new Point(px3, py3),
                            new Point(px4, py4)
                            ));
                        i++;
                    }

                }
                catch (Exception)
                {
                }
            }
            public double GetPropertyValue(string property)
            {
                double propertyValue = 0;
                double dist = 0;
                OCRComponent theClosestComponent = null;
                foreach (OCRComponent component in listOfComponents)
                {
                    if (component.description.ToLower() == property.ToLower())
                    {
                        dist = int.MaxValue;
                        double a1 = 0;
                        double b1 = 0;
                        double a2 = 0;
                        double b2 = 0;
                        foreach (OCRComponent nextComponent in listOfComponents)
                        {
                            if (nextComponent != component)
                            {
                                a1 = (component.point2.Y - component.point1.Y) / (component.point2.X - component.point1.X);
                                b1 = component.point1.Y - a1 * component.point1.X;
                                a2 = (component.point3.Y - component.point4.Y) / (component.point3.X - component.point4.X);
                                b2 = component.point4.Y - a1 * component.point4.X;
                                double yP = (nextComponent.point1.Y + nextComponent.point2.Y + nextComponent.point3.Y + nextComponent.point4.Y) / 4;
                                double xP = (nextComponent.point1.X + nextComponent.point2.X + nextComponent.point3.X + nextComponent.point4.X) / 4;
                                if (a1 * xP + b1 < yP && a2 * xP + b2 > yP)
                                {
                                    double diff = xP - component.point3.X;
                                    if (diff < dist && diff > 0)
                                    {
                                        theClosestComponent = nextComponent;
                                        dist = diff;
                                    }
                                }

                            }
                        }
                    }
                }
                try
                {
                    propertyValue = ConvertToDouble(RemoveIncorrentCharacters(theClosestComponent.description));
                }
                catch(NullReferenceException)
                {
                }
                return propertyValue;
            }
        }
    }
}
