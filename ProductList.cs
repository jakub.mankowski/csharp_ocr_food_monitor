﻿using System.Collections.Generic;

namespace ProjectOCR
{
    partial class Program
    {
        public class ProductList
        {
            private List<Product> listOfProducts;
            public ProductList()
            {
                listOfProducts = new List<Product>();
            }
            public void Add(Product p)
            {
                listOfProducts.Add(p);
            }
            public Product CalculateProductsTotal()
            {
                Product total = new Product(1);
                foreach (Product p in listOfProducts)
                {
                    total.sugar += p.sugar;
                    total.fat += p.fat;
                    total.carbohydrates += p.carbohydrates;
                }
                return total;
            }
            public string DisplayAllProducts()
            {
                string allProductsInfo = "";
                foreach (Product p in listOfProducts)
                {
                    allProductsInfo += @"Product mass: " + p.mass + "g\n" + p.getProductInfo();
                }
                return allProductsInfo;
            }
        }
    }
}
